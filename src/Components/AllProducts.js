import React, { useState, useEffect } from "react";
import Card from "./Card";
function AllProducts() {
  const [allProducts, setAllProducts] = useState([]);
  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((json) => setAllProducts(json));
  }, []);

  return (
    <div>
      {allProducts.length !== 0 ? (
        <div>
          {allProducts.map((data) => (
            <Card data={data} />
          ))}
        </div>
      ) : (
        <div>Loading</div>
      )}
    </div>
  );
}

export default AllProducts;
