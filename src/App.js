import logo from "./logo.svg";
import "./App.css";
import AllProducts from "./Components/AllProducts";
import ResponsiveAppBar from "./Components/AppBar";
import { Container } from "@mui/material";

function App() {
  return (
    <Container maxWidth="lg">
      <ResponsiveAppBar />
      <AllProducts />
    </Container>
  );
}

export default App;
